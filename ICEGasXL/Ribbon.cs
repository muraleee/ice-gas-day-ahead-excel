﻿using ExcelDna.Integration.CustomUI;
using ExcelDna.Integration;
using System.Xml;
using System.IO;
using System.Reflection;
using System.Runtime.InteropServices;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using System.Collections.Generic;
using ICEGasXL.GUI;
using ICEGasXL.Utility;
using System;
using System.Windows.Forms;

namespace ICEGasXL
{
    [ComVisible(true)]
    public class Ribbon : ExcelRibbon, IExcelAddIn
    {
        public override string GetCustomUI(string RibbonID)
        {
            XmlDocument doc = new XmlDocument();
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("ICEGasXL.Ribbon.xml");
            doc.Load(stream);


            return doc.InnerXml;
        }

        public void AboutButton_Click(IRibbonControl control)
        {
            AboutBox about = new AboutBox();
            about.ShowDialog();
        }


        public void RetrievalDateSelection_Click(IRibbonControl control)
        {
            RetrievalDateDialog dateDialog = new RetrievalDateDialog();
            dateDialog.ShowDialog();

        }

       
        public void DownloadButton_Click(IRibbonControl control)
        {
            RetrievalDateDialog dateDialog = new RetrievalDateDialog();
            dateDialog.ShowDialog();

            ICEDAGasProcessor processor = new ICEDAGasProcessor();
            Excel.Application app = (Excel.Application)ExcelDnaUtil.Application;
            app.Cursor = XlMousePointer.xlWait;


            List<string> outputList = processor.Process(GasXLUtility.RetrievalDate ?? DateTime.Now);

            if (outputList.Count == 0)
            {
                MessageBox.Show("No Data available for selected date.", "ICE Gas Data");
                app.Cursor = XlMousePointer.xlDefault;
                return;
            }

            int numColumns = outputList[0].Split(new char[] { ',' }).Length;
            int numRows = outputList.Count;

            if (app.ActiveWorkbook == null)
            {
                app.Workbooks.Add();
            }
            Worksheet sheet = (Worksheet)app.ActiveWorkbook.Sheets.Add();

            Range range = sheet.get_Range("A1");

         
            app.ScreenUpdating = false;
         

            for (int rows = 0; rows < numRows; rows++)
            {
                string[] columnData = outputList[rows].Split(new char[] { ',' });
                for (int columns = 0; columns < columnData.Length; columns++)
                {
                    Range r = range.get_Offset(rows, columns);
                    r.Value = columnData[columns];
                   
                }
            }

            
            app.ScreenUpdating = true;
            app.Cursor = XlMousePointer.xlDefault;
        }

        public void AutoClose()
        {
           
        }

        public void AutoOpen()
        {
            
        }

        public string List { get; set; }
    }
}
