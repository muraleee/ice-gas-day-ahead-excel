﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using HtmlAgilityPack;
using System.Text.RegularExpressions;
using System.Globalization;

namespace ICEGasXL
{
    public class ICEDAGasProcessor
    {
        string iceUrl = "https://www.theice.com/marketdata/indices/gasResults.do?indexType=1&hubChoice=All&startDay={0}&startMonth={1}&startYear={2}&endDay={3}&endMonth={4}&endYear={5}";
         
        private string Retrieve(string url)
        {
            WebClient client = new WebClient();
            return client.DownloadString(url);
        }

        private List<string> Parse(string iceDAHtml)
        {
            List<string> outputList = new List<string>();


            HtmlDocument doc = new HtmlDocument();

            doc.LoadHtml(iceDAHtml);

            int tableCount = 0;

            foreach (HtmlNode table in doc.DocumentNode.SelectNodes("//table"))
            {
                if (tableCount == 2)
                {
                    int rowPosition = 0;
                    foreach (HtmlNode row in table.SelectNodes(".//tr"))
                    {
                        StringBuilder rowStringBuilder = new StringBuilder();

                        int columnPosition = 0;
                       

                        foreach (HtmlNode cell in row.SelectNodes("th|td"))
                        {
                            string text = Regex.Replace(cell.InnerText, @"\s+", " ", RegexOptions.Multiline);

                            if (rowPosition > 0 && (columnPosition == 1 || columnPosition == 2 || columnPosition == 3))
                            {
                                if(text.Trim().Contains("EDT"))
                                    text = DateTime.ParseExact(text.Trim(), "ddd MMM dd hh:mm:ss EDT yyyy", CultureInfo.CurrentCulture).ToString("d");
                                else
                                    text = DateTime.ParseExact(text.Trim(), "ddd MMM dd hh:mm:ss EST yyyy", CultureInfo.CurrentCulture).ToString("d");
                            }
                            rowStringBuilder.Append(text);
                            rowStringBuilder.Append(",");

                            columnPosition++;
                        }

                        outputList.Add(rowStringBuilder.ToString());
                        rowPosition++;
                    }

                }
                tableCount++;
            }

            return outputList;
        }

        public List<String> Process(DateTime requestDate)
        {
            string url = String.Format(iceUrl, requestDate.Day, requestDate.Month - 1, requestDate.Year, requestDate.Day, requestDate.Month - 1, requestDate.Year);
            string data = Retrieve(url);
            return Parse(data);
        }
    }
}
