﻿using System;
using System.Windows.Forms;
using ICEGasXL.Utility;

namespace ICEGasXL.GUI
{
    public partial class RetrievalDateDialog : Form
    {
        public RetrievalDateDialog()
        {
            InitializeComponent();
        }

       

        private void FormMarkDate_Load(object sender, EventArgs e)
        {
            dateTimePicker1.MaxDate = DateTime.Now;

            try
            {
                if (GasXLUtility.RetrievalDate != null) 
                {
                    if (GasXLUtility.RetrievalDate.Value.Date == DateTime.Today.Date)
                        defaulltCheckBox.Checked = true;
                    else
                        defaulltCheckBox.Checked = false;

                    dateTimePicker1.Value = GasXLUtility.RetrievalDate.Value;
                }
                else
                {
                    defaulltCheckBox.Checked = true;
                    dateTimePicker1.Value = DateTime.Now;
                }
            }
            catch (Exception)
            {
                dateTimePicker1.Value = DateTime.Now;
            }
        }

    

        private void okButton_Click(object sender, EventArgs e)
        {
            GasXLUtility.RetrievalDate = dateTimePicker1.Value;

            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void defaulltCheckBox_CheckedChanged(object sender, EventArgs e)
        {
            if (defaulltCheckBox.Enabled)
            {
                dateTimePicker1.Enabled = true;
                dateTimePicker1.Value = DateTime.Today;
            }
        }
    }
}
